import Renderer from './Renderer.js';
import Game from './Game.js';

const canvasEl = document.querySelector('canvas');
const scoreEl = document.querySelector('#score');
const renderer = new Renderer(canvasEl);
const game = new Game(renderer);

resize();
game.init();
window.addEventListener('resize', resize);
game.on('score:change', updateScore);
window.requestAnimationFrame(loop);

/**
 * Resizes canvas to make it fits its container.
 */
function resize() {
  const w = canvasEl.parentElement.offsetWidth;
  renderer.resize(w, w);
}

/**
 * Main loop.
 * Updates elements and then reder them.
 */
function loop() {
  game.update();
  renderer.render();
  window.requestAnimationFrame(loop);
}

/**
 * Displays score to user.
 */
function updateScore(value) {
  scoreEl.innerHTML = Math.floor(value);
}
