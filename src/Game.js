import EventEmitter from './utils/EventEmitter.js';

export default class Game extends EventEmitter {
  constructor(renderer) {
    super();

    this.renderer = renderer;
    this.score = 0;
  }

  init() {}

  addPoints(points) {
    this.score += points;
    this.emit('score:change', this.score);
  }

  removePoints(points) {
    this.score -= points;
    this.emit('score:change', this.score);
  }

  update() {}
}
