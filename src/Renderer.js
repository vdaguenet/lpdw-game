export default class Renderer {
  constructor(el) {
    this.canvas = el;
    this.ctx = el.getContext('2d');
    this.objects = [];
  }

  get width() {
    return this.canvas.width;
  }

  get height() {
    return this.canvas.height;
  }

  resize(w, h) {
    this.canvas.width = w;
    this.canvas.height = h;
  }

  render() {
    this.objects.forEach((obj) => {
      obj.render(this.ctx);
    });
  }
}
