export default class Point {
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  setX(x) {
    this.x = x;
  }

  setY(y) {
    this.y = y;
  }

  set(x, y) {
    this.x = x;
    this.y = y;
  }

  add(x, y) {
    this.x += x;
    this.y += y;
  }

  addPoint(p) {
    this.x += p.x;
    this.y += p.y;
  }

  sub(x, y) {
    this.x -= x;
    this.y -= y;
  }

  subPoint(p) {
    this.x -= p.x;
    this.y -= p.y;
  }

  scale(k) {
    this.x *= k;
    this.y *= k;
  }
}
