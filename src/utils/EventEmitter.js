export default class EventEmitter {
  constructor() {
    this.events = Object.create(null);
  }

  /**
   * Registers listener to given event.
   */
  on(event, cb) {
    if (!this.events[event]) {
      this.events[event] = [];
    }

    this.events[event].push(cb);
  }

  /**
   * Executes listeners of given event.
   */
  emit(event, ...args) {
    if (!this.events[event]) return;

    this.events[event].forEach(listener => listener && listener(args));
  }
}
