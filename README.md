# lpdw-game

Lesson about game development in JavaScript.

## Installation
After having cloned the repositry:
```bash
cd lpdw-game
rm -rf ./.git
npm install
```

## Developing
```bash
npm start
```
Will build the JavaScript files and start a dev server with live reloading available at this address: [localhost:8080](http://localhost:8080).